package ncrb.droid.locatepolicestation;

import java.util.List;

/**
 * Created by sez1 on 24/11/15.
 */

public class WSPLoadMapFindPSConnect {
    
    public List<PoliceStationLists> delhi_ps;

    public List<PoliceStationLists> getPSLists() {
        return delhi_ps;
    }

    public void setPSLists(List<PoliceStationLists> PSLists) {
        this.delhi_ps = PSLists;
    }


    public class PoliceStationLists {

        public String PS_Name;
        public String District;
        public String State_UT_Name;
        public String Lat;
        public String Long;

    }// end PoliceStationLists

}// end main class
