package ncrb.droid.locatepolicestation;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.maps.GoogleMap;
import com.google.gson.Gson;

import java.util.HashMap;

public class LoadMapDetailFindPSActivity extends Activity {

    HashMap<String, String> latlng = new HashMap<String, String>();
    TextView tv_address;
    TextView tv_phone;
    TextView tv_distance;
    // Google Map
    private GoogleMap googleMap;

    public static final int MY_PERMISSIONS_REQUEST_CALL = 99;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load_map_detail_find_ps);

        String userJson = Utils.loadJSONFromAsset(this, "ps_list.json");

        Utils.printv("userJson " + userJson);
        Gson gson = new Gson();
        WSPLoadMapFindPSConnect userObject = gson.fromJson(userJson, WSPLoadMapFindPSConnect.class);

        tv_address = (TextView) findViewById(R.id.tv_address);
        tv_phone = (TextView) findViewById(R.id.tv_phone);
        tv_distance = (TextView) findViewById(R.id.tv_distance);

        int array_index = Integer.parseInt(getIntent().getStringExtra("ARRAY_POSITION"));
        //LatLng objLatLng = getIntent().getStringExtra("LATLNG");

        tv_address.setText("" + userObject.delhi_ps.get(array_index).PS_Name + " , " + userObject.delhi_ps.get(array_index).District
                + " , " + userObject.delhi_ps.get(array_index).State_UT_Name);

        tv_phone.setText("01126172324");

        final String[] ps_latLng = getIntent().getStringExtra("LATLNG").split(",");
        final String[] curnt_latLng = getIntent().getStringExtra("CURNT_LATLNG").split(",");

        double distance = Utils.distanceBWLatLng(Double.parseDouble(ps_latLng[0]), Double.parseDouble(ps_latLng[0]),
                Double.parseDouble(curnt_latLng[0]), Double.parseDouble(curnt_latLng[0]));
        tv_distance.setText(String.format("%.2f", distance) + " KM");


        final Button button = (Button) findViewById(R.id.call_ps_btn);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                try {
                    if (checkCallPermission()) {
                        Intent in = new Intent(Intent.ACTION_CALL, Uri.parse("tel:01126172324"));
                        startActivity(in);
                    }

                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(LoadMapDetailFindPSActivity.this, "Could not find an activity to place the call.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        final Button btn_direction = (Button) findViewById(R.id.view_direction_btn);
        btn_direction.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse("http://maps.google.com/maps?saddr=" + curnt_latLng[0] + "," + curnt_latLng[1] + "&" + "daddr=" + ps_latLng[0] + "," + ps_latLng[1]));
                    startActivity(intent);
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(LoadMapDetailFindPSActivity.this, "Maps couldn't open.", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }// end oncreate

    @Override
    protected void onResume() {
        super.onResume();

    }

    public boolean checkCallPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.CALL_PHONE)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                        .setTitle("Call Permission")
                        .setMessage("Please allow call permission.")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(LoadMapDetailFindPSActivity.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_CALL);
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CALL_PHONE},
                        MY_PERMISSIONS_REQUEST_CALL);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.CALL_PHONE)
                            == PackageManager.PERMISSION_GRANTED) {

                        //Request location updates:
                        //appLocationService.requestLocationUpdates(provider, 400, 1, this);
                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.

                }
                return;
            }

        }
    }

}// end main activity
