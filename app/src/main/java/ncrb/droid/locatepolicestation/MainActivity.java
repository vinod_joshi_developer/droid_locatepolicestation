package ncrb.droid.locatepolicestation;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Button button = (Button) findViewById(R.id.open_map);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                try {

                    Intent intent = new Intent(getBaseContext(), LoadMapFindPSActivity.class);
                    startActivity(intent);


                } catch (Exception ex) {

                }
            }
        });

    }
}