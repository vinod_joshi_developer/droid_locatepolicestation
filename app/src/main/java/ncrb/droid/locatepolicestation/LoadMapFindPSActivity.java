package ncrb.droid.locatepolicestation;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;

import java.util.HashMap;

public class LoadMapFindPSActivity extends Activity {

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    // Google Map
    private GoogleMap googleMap;

    HashMap<String, String> latlng = new HashMap<String, String>();
    AppLocationService appLocationService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load_map_find_ps);

        appLocationService = new AppLocationService(LoadMapFindPSActivity.this);

        if (checkLocationPermission()) initilizeMap();


    }// end oncreate

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                        .setTitle("Location Permission")
                        .setMessage("Please allow location permission.")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(LoadMapFindPSActivity.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        //Request location updates:
                        //appLocationService.requestLocationUpdates(provider, 400, 1, this);
                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.

                }
                return;
            }

        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {

            //locationManager.requestLocationUpdates(provider, 400, 1, this);
        }

        //initilizeMap();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {

            //locationManager.removeUpdates(this);
        }
    }


    /**
     * function to load map If map is not created it will create it for you
     */
    private void initilizeMap() {

        if (googleMap == null) {

            googleMap = ((MapFragment) getFragmentManager().findFragmentById(
                    R.id.map)).getMap();

            // check if map is created successfully or not
            if (googleMap == null) {
                Toast.makeText(getApplicationContext(),
                        "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                        .show();
            }// end if
            else {
                loadmap();
            }
        }// end if

    }// end initilize map

    private void loadmap() {

        try {

            // Changing map type
            googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            // googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
            // googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
            // googleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
            // googleMap.setMapType(GoogleMap.MAP_TYPE_NONE);

            // Showing / hiding your current location
            googleMap.setMyLocationEnabled(true);

            // Enable / Disable zooming controls
            googleMap.getUiSettings().setZoomControlsEnabled(false);

            // Enable / Disable my location button
            googleMap.getUiSettings().setMyLocationButtonEnabled(true);

            // Enable / Disable Compass icon
            googleMap.getUiSettings().setCompassEnabled(true);

            // Enable / Disable Rotate gesture
            googleMap.getUiSettings().setRotateGesturesEnabled(true);

            // Enable / Disable zooming functionality
            googleMap.getUiSettings().setZoomGesturesEnabled(true);

            // demo codes = show demo location if network setting not correct
            double latitude = 28.6300686111111;
            double longitude = 77.0679233333333;

            // if need server json value = https://api.myjson.com/bins/1e3t09
            String userJson = Utils.loadJSONFromAsset(this, "ps_list.json");

            Utils.printv("userJson " + userJson);
            Gson gson = new Gson();
            WSPLoadMapFindPSConnect userObject = gson.fromJson(userJson, WSPLoadMapFindPSConnect.class);

            Utils.printv("userObject.delhi_ps.size() " + userObject.delhi_ps.size());

            // lets place some 10 random markers
            for (int i = 0; i < userObject.delhi_ps.size(); i++) {

                Utils.printv("vj district " + userObject.delhi_ps.get(i).District);
                Utils.printv("vj lat " + userObject.delhi_ps.get(i).Lat);
                Utils.printv("vj lng " + userObject.delhi_ps.get(i).Long);
                Utils.printv("vj i " + i);

                if (userObject.delhi_ps.get(i).Lat == null || userObject.delhi_ps.get(i).Long == null) {
                    Utils.printv("vj continue null value");
                    continue;
                }

                double lat = Double.parseDouble(userObject.delhi_ps.get(i).Lat);
                double lng = Double.parseDouble(userObject.delhi_ps.get(i).Long);

                Utils.printv("vj lat " + lat + " lng " + lng);

                // random latitude and logitude
                //double[] randomLocation = createRandLocation(latitude, longitude);
                double[] randomLocation = new double[]{lat, lng, 150 + ((Math.random() - 0.5) * 10)};

                // Adding a marker
                // title = ""+userObject.delhi_ps.get(i).PS_Name + " , "+ userObject.delhi_ps.get(i).District
                //+ " , " + userObject.delhi_ps.get(i).State_UT_Name

                MarkerOptions marker = new MarkerOptions().position(
                        new LatLng(randomLocation[0], randomLocation[1]))
                        .title("" + i);

                Log.e("Random", "> " + randomLocation[0] + ", "
                        + randomLocation[1]);

                marker.icon(BitmapDescriptorFactory
                        .defaultMarker(BitmapDescriptorFactory.HUE_RED));


                latlng = Utils.getCurrentLatLng(LoadMapFindPSActivity.this, appLocationService);

                googleMap.addMarker(marker);

                googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {

                    @Override
                    public boolean onMarkerClick(Marker arg0) {

                        //Toast.makeText(LoadMapFindPSActivity.this, arg0.getId()+" Lat "+arg0.getPosition(), Toast.LENGTH_SHORT).show();// display toast

                        Intent intent = new Intent(getBaseContext(), LoadMapDetailFindPSActivity.class);
                        intent.putExtra("LATLNG", arg0.getPosition().latitude + "," + arg0.getPosition().longitude);
                        intent.putExtra("CURNT_LATLNG", latlng.get("lat") + "," + latlng.get("lng"));
                        intent.putExtra("ARRAY_POSITION", "" + arg0.getTitle());
                        intent.putExtra("ID", "" + arg0.getId());
                        startActivity(intent);

                        return true;
                    }

                });


                if (latlng.get("network_gps_enabled").equals("true")) {

                    // show current location of user
                    CameraPosition cameraPosition = new CameraPosition.Builder()
                            .target(new LatLng(Double.parseDouble(latlng.get("lat")),
                                    Double.parseDouble(latlng.get("lng")))).zoom(15).build();

                    googleMap.animateCamera(CameraUpdateFactory
                            .newCameraPosition(cameraPosition));
                } else {

                    // show demo location
                    CameraPosition cameraPosition = new CameraPosition.Builder()
                            .target(new LatLng(latitude, longitude)).zoom(15).build();
                    googleMap.animateCamera(CameraUpdateFactory
                            .newCameraPosition(cameraPosition));

                    Utils.showAlert(LoadMapFindPSActivity.this, "Either location permission not given to app or GPS location not available. " +
                            "\n\nPlease allow location permission. i.e. Settings > App > Permissions.");
                }

            }// end for loop plot icons on map

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
     * creating random postion around a location for testing purpose only
     */
    private double[] createRandLocation(double latitude, double longitude) {

        return new double[]{latitude + ((Math.random() - 0.5) / 500),
                longitude + ((Math.random() - 0.5) / 500),
                150 + ((Math.random() - 0.5) * 10)};
    }// end create rand location

}// end main activity
